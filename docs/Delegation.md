# TezexClient::Delegation

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**hash** | **String** |  | [optional] 
**branch** | **String** |  | [optional] 
**source** | **String** |  | [optional] 
**public_key** | **String** |  | [optional] 
**fee** | **Integer** |  | [optional] 
**counter** | **Integer** |  | [optional] 
**delegate** | **String** |  | [optional] 
**level** | **Integer** |  | [optional] 
**block_hash** | **String** |  | [optional] 
**time** | **DateTime** |  | [optional] 


