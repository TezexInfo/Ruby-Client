# TezexClient::Block

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**hash** | **String** |  | [optional] 
**net_id** | **String** |  | [optional] 
**protocol** | **String** |  | [optional] 
**level** | **Integer** |  | [optional] 
**proto** | **String** |  | [optional] 
**successors** | [**Array&lt;ChainStatus&gt;**](ChainStatus.md) |  | [optional] 
**predecessor** | **String** |  | [optional] 
**time** | **DateTime** |  | [optional] 
**validation_pass** | **String** |  | [optional] 
**data** | **String** |  | [optional] 
**chain_status** | **String** |  | [optional] 
**operations_count** | **Integer** |  | [optional] 
**operations_hash** | **String** |  | [optional] 
**baker** | **String** |  | [optional] 
**seed_nonce_hash** | **String** |  | [optional] 
**proof_of_work_nonce** | **String** |  | [optional] 
**signature** | **String** |  | [optional] 
**priority** | **Integer** |  | [optional] 
**operation_count** | **Integer** |  | [optional] 
**total_fee** | **String** |  | [optional] 
**operations** | [**BlockOperationsSorted**](BlockOperationsSorted.md) |  | [optional] 


