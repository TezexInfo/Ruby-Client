# TezexClient::NetworkInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**max_level** | **Integer** |  | [optional] 
**blocktime** | **String** |  | [optional] 
**transactions_24h** | **Integer** |  | [optional] 
**oeprations_24h** | **String** |  | [optional] 


