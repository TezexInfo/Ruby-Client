# TezexClient::EndorsementApi

All URIs are relative to *http://betaapi.tezex.info/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_endorsement**](EndorsementApi.md#get_endorsement) | **GET** /endorsement/{endorsement_hash} | Get Endorsement
[**get_endorsement_for_block**](EndorsementApi.md#get_endorsement_for_block) | **GET** /endorsement/for/{block_hash} | Get Endorsement


# **get_endorsement**
> Endorsement get_endorsement(endorsement_hash)

Get Endorsement

Get a specific Endorsement

### Example
```ruby
# load the gem
require 'tezex_client'

api_instance = TezexClient::EndorsementApi.new

endorsement_hash = "endorsement_hash_example" # String | The hash of the Endorsement to retrieve


begin
  #Get Endorsement
  result = api_instance.get_endorsement(endorsement_hash)
  p result
rescue TezexClient::ApiError => e
  puts "Exception when calling EndorsementApi->get_endorsement: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **endorsement_hash** | **String**| The hash of the Endorsement to retrieve | 

### Return type

[**Endorsement**](Endorsement.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json



# **get_endorsement_for_block**
> Array&lt;Endorsement&gt; get_endorsement_for_block(block_hash)

Get Endorsement

Get a specific Endorsement

### Example
```ruby
# load the gem
require 'tezex_client'

api_instance = TezexClient::EndorsementApi.new

block_hash = "block_hash_example" # String | blockhash


begin
  #Get Endorsement
  result = api_instance.get_endorsement_for_block(block_hash)
  p result
rescue TezexClient::ApiError => e
  puts "Exception when calling EndorsementApi->get_endorsement_for_block: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **block_hash** | **String**| blockhash | 

### Return type

[**Array&lt;Endorsement&gt;**](Endorsement.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json



