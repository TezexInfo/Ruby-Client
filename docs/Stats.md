# TezexClient::Stats

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**stat_group** | **String** |  | [optional] 
**stat** | **String** |  | [optional] 
**value** | **String** |  | [optional] 
**start** | **DateTime** |  | [optional] 
**_end** | **DateTime** |  | [optional] 


