# TezexClient::TransactionRange

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**max_block_level** | **Integer** |  | [optional] 
**transactions** | [**Array&lt;Transaction&gt;**](Transaction.md) |  | [optional] 
**total_results** | **Integer** |  | [optional] 
**current_page** | **Integer** |  | [optional] 


