# TezexClient::MarketApi

All URIs are relative to *http://betaapi.tezex.info/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**candlestick**](MarketApi.md#candlestick) | **GET** /price/{denominator}/{numerator}/{period} | Candlestick Data
[**ticker**](MarketApi.md#ticker) | **GET** /ticker/{numerator} | Get Ticker for a specific Currency


# **candlestick**
> Array&lt;Candlestick&gt; candlestick(denominator, numerator, period)

Candlestick Data

Returns CandleStick Prices

### Example
```ruby
# load the gem
require 'tezex_client'

api_instance = TezexClient::MarketApi.new

denominator = "denominator_example" # String | which currency

numerator = "numerator_example" # String | to which currency

period = "period_example" # String | Timeframe of one candle


begin
  #Candlestick Data
  result = api_instance.candlestick(denominator, numerator, period)
  p result
rescue TezexClient::ApiError => e
  puts "Exception when calling MarketApi->candlestick: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **denominator** | **String**| which currency | 
 **numerator** | **String**| to which currency | 
 **period** | **String**| Timeframe of one candle | 

### Return type

[**Array&lt;Candlestick&gt;**](Candlestick.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json



# **ticker**
> Ticker ticker(numerator)

Get Ticker for a specific Currency

Returns BTC, USD, EUR and CNY Prices

### Example
```ruby
# load the gem
require 'tezex_client'

api_instance = TezexClient::MarketApi.new

numerator = "numerator_example" # String | The level of the Blocks to retrieve


begin
  #Get Ticker for a specific Currency
  result = api_instance.ticker(numerator)
  p result
rescue TezexClient::ApiError => e
  puts "Exception when calling MarketApi->ticker: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **numerator** | **String**| The level of the Blocks to retrieve | 

### Return type

[**Ticker**](Ticker.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json



