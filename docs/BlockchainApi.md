# TezexClient::BlockchainApi

All URIs are relative to *http://betaapi.tezex.info/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**blockheight**](BlockchainApi.md#blockheight) | **GET** /maxLevel | Get Max Blockheight


# **blockheight**
> Level blockheight

Get Max Blockheight

Get the maximum Level we have seen

### Example
```ruby
# load the gem
require 'tezex_client'

api_instance = TezexClient::BlockchainApi.new

begin
  #Get Max Blockheight
  result = api_instance.blockheight
  p result
rescue TezexClient::ApiError => e
  puts "Exception when calling BlockchainApi->blockheight: #{e}"
end
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**Level**](Level.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json



