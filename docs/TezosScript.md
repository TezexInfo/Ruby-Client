# TezexClient::TezosScript

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**int** | **String** |  | [optional] 
**string** | **String** |  | [optional] 
**prim** | **String** |  | [optional] 
**args** | [**Array&lt;TezosScript&gt;**](TezosScript.md) |  | [optional] 


