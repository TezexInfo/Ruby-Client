# TezexClient::AccountApi

All URIs are relative to *http://betaapi.tezex.info/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_account**](AccountApi.md#get_account) | **GET** /account/{account} | Get Account
[**get_account_balance**](AccountApi.md#get_account_balance) | **GET** /account/{account}/balance | Get Account Balance
[**get_account_last_seen**](AccountApi.md#get_account_last_seen) | **GET** /account/{account}/last_seen | Get last active date
[**get_account_operation_count**](AccountApi.md#get_account_operation_count) | **GET** /account/{account}/operations_count | Get operation count of Account
[**get_account_transaction_count**](AccountApi.md#get_account_transaction_count) | **GET** /account/{account}/transaction_count | Get transaction count of Account
[**get_delegations_for_account**](AccountApi.md#get_delegations_for_account) | **GET** /account/{account}/delegations | Get Delegations of this account
[**get_delegations_to_account**](AccountApi.md#get_delegations_to_account) | **GET** /account/{account}/delegated | Get Delegations to this account
[**get_endorsements_for_account**](AccountApi.md#get_endorsements_for_account) | **GET** /account/{account}/endorsements | Get Endorsements this Account has made
[**get_transaction_for_account_incoming**](AccountApi.md#get_transaction_for_account_incoming) | **GET** /account/{account}/transactions/incoming | Get Transaction
[**get_transaction_for_account_outgoing**](AccountApi.md#get_transaction_for_account_outgoing) | **GET** /account/{account}/transactions/outgoing | Get Transaction


# **get_account**
> Account get_account(account)

Get Account

Get Acccount

### Example
```ruby
# load the gem
require 'tezex_client'

api_instance = TezexClient::AccountApi.new

account = "account_example" # String | The account


begin
  #Get Account
  result = api_instance.get_account(account)
  p result
rescue TezexClient::ApiError => e
  puts "Exception when calling AccountApi->get_account: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account** | **String**| The account | 

### Return type

[**Account**](Account.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json



# **get_account_balance**
> String get_account_balance(account)

Get Account Balance

Get Balance

### Example
```ruby
# load the gem
require 'tezex_client'

api_instance = TezexClient::AccountApi.new

account = "account_example" # String | The account


begin
  #Get Account Balance
  result = api_instance.get_account_balance(account)
  p result
rescue TezexClient::ApiError => e
  puts "Exception when calling AccountApi->get_account_balance: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account** | **String**| The account | 

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json



# **get_account_last_seen**
> DateTime get_account_last_seen(account)

Get last active date

Get LastSeen Date

### Example
```ruby
# load the gem
require 'tezex_client'

api_instance = TezexClient::AccountApi.new

account = "account_example" # String | The account


begin
  #Get last active date
  result = api_instance.get_account_last_seen(account)
  p result
rescue TezexClient::ApiError => e
  puts "Exception when calling AccountApi->get_account_last_seen: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account** | **String**| The account | 

### Return type

**DateTime**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json



# **get_account_operation_count**
> Integer get_account_operation_count(account)

Get operation count of Account

Get Operation Count

### Example
```ruby
# load the gem
require 'tezex_client'

api_instance = TezexClient::AccountApi.new

account = "account_example" # String | The account


begin
  #Get operation count of Account
  result = api_instance.get_account_operation_count(account)
  p result
rescue TezexClient::ApiError => e
  puts "Exception when calling AccountApi->get_account_operation_count: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account** | **String**| The account | 

### Return type

**Integer**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json



# **get_account_transaction_count**
> Integer get_account_transaction_count(account)

Get transaction count of Account

Get Transaction Count

### Example
```ruby
# load the gem
require 'tezex_client'

api_instance = TezexClient::AccountApi.new

account = "account_example" # String | The account


begin
  #Get transaction count of Account
  result = api_instance.get_account_transaction_count(account)
  p result
rescue TezexClient::ApiError => e
  puts "Exception when calling AccountApi->get_account_transaction_count: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account** | **String**| The account | 

### Return type

**Integer**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json



# **get_delegations_for_account**
> Array&lt;Delegation&gt; get_delegations_for_account(account, opts)

Get Delegations of this account

Get Delegations this Account has made

### Example
```ruby
# load the gem
require 'tezex_client'

api_instance = TezexClient::AccountApi.new

account = "account_example" # String | The account for which to retrieve Delegations

opts = { 
  before: 56 # Integer | Only Return Delegations before this blocklevel
}

begin
  #Get Delegations of this account
  result = api_instance.get_delegations_for_account(account, opts)
  p result
rescue TezexClient::ApiError => e
  puts "Exception when calling AccountApi->get_delegations_for_account: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account** | **String**| The account for which to retrieve Delegations | 
 **before** | **Integer**| Only Return Delegations before this blocklevel | [optional] 

### Return type

[**Array&lt;Delegation&gt;**](Delegation.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json



# **get_delegations_to_account**
> Array&lt;Delegation&gt; get_delegations_to_account(account, opts)

Get Delegations to this account

Get that have been made to this Account

### Example
```ruby
# load the gem
require 'tezex_client'

api_instance = TezexClient::AccountApi.new

account = "account_example" # String | The account to which delegations have been made

opts = { 
  before: 56 # Integer | Only Return Delegations before this blocklevel
}

begin
  #Get Delegations to this account
  result = api_instance.get_delegations_to_account(account, opts)
  p result
rescue TezexClient::ApiError => e
  puts "Exception when calling AccountApi->get_delegations_to_account: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account** | **String**| The account to which delegations have been made | 
 **before** | **Integer**| Only Return Delegations before this blocklevel | [optional] 

### Return type

[**Array&lt;Delegation&gt;**](Delegation.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json



# **get_endorsements_for_account**
> Array&lt;Endorsement&gt; get_endorsements_for_account(account, opts)

Get Endorsements this Account has made

Get Endorsements this Account has made

### Example
```ruby
# load the gem
require 'tezex_client'

api_instance = TezexClient::AccountApi.new

account = "account_example" # String | The account for which to retrieve Endorsements

opts = { 
  before: 56 # Integer | Only Return Delegations before this blocklevel
}

begin
  #Get Endorsements this Account has made
  result = api_instance.get_endorsements_for_account(account, opts)
  p result
rescue TezexClient::ApiError => e
  puts "Exception when calling AccountApi->get_endorsements_for_account: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account** | **String**| The account for which to retrieve Endorsements | 
 **before** | **Integer**| Only Return Delegations before this blocklevel | [optional] 

### Return type

[**Array&lt;Endorsement&gt;**](Endorsement.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json



# **get_transaction_for_account_incoming**
> Transactions get_transaction_for_account_incoming(account, opts)

Get Transaction

Get incoming Transactions for a specific Account

### Example
```ruby
# load the gem
require 'tezex_client'

api_instance = TezexClient::AccountApi.new

account = "account_example" # String | The account for which to retrieve incoming Transactions

opts = { 
  before: 56 # Integer | Only Return transactions before this blocklevel
}

begin
  #Get Transaction
  result = api_instance.get_transaction_for_account_incoming(account, opts)
  p result
rescue TezexClient::ApiError => e
  puts "Exception when calling AccountApi->get_transaction_for_account_incoming: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account** | **String**| The account for which to retrieve incoming Transactions | 
 **before** | **Integer**| Only Return transactions before this blocklevel | [optional] 

### Return type

[**Transactions**](Transactions.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json



# **get_transaction_for_account_outgoing**
> Transactions get_transaction_for_account_outgoing(account, opts)

Get Transaction

Get outgoing Transactions for a specific Account

### Example
```ruby
# load the gem
require 'tezex_client'

api_instance = TezexClient::AccountApi.new

account = "account_example" # String | The account for which to retrieve outgoing Transactions

opts = { 
  before: 56 # Integer | Only return transactions before this blocklevel
}

begin
  #Get Transaction
  result = api_instance.get_transaction_for_account_outgoing(account, opts)
  p result
rescue TezexClient::ApiError => e
  puts "Exception when calling AccountApi->get_transaction_for_account_outgoing: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account** | **String**| The account for which to retrieve outgoing Transactions | 
 **before** | **Integer**| Only return transactions before this blocklevel | [optional] 

### Return type

[**Transactions**](Transactions.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json



