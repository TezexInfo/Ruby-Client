# TezexClient::OriginationApi

All URIs are relative to *http://betaapi.tezex.info/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_origination**](OriginationApi.md#get_origination) | **GET** /origination/{origination_hash} | Get Origination


# **get_origination**
> Origination get_origination(origination_hash)

Get Origination

Get a specific Origination

### Example
```ruby
# load the gem
require 'tezex_client'

api_instance = TezexClient::OriginationApi.new

origination_hash = "origination_hash_example" # String | The hash of the Origination to retrieve


begin
  #Get Origination
  result = api_instance.get_origination(origination_hash)
  p result
rescue TezexClient::ApiError => e
  puts "Exception when calling OriginationApi->get_origination: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **origination_hash** | **String**| The hash of the Origination to retrieve | 

### Return type

[**Origination**](Origination.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json



