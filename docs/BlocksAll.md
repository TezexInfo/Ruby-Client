# TezexClient::BlocksAll

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**max_block_level** | **Integer** |  | [optional] 
**blocks** | [**Array&lt;Block&gt;**](Block.md) |  | [optional] 
**total_results** | **Integer** |  | [optional] 
**current_page** | **Integer** |  | [optional] 


