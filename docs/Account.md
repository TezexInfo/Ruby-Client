# TezexClient::Account

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**address** | **String** |  | [optional] 
**operation_count** | **Integer** |  | [optional] 
**sent_transaction_count** | **Integer** |  | [optional] 
**recv_transaction_count** | **Integer** |  | [optional] 
**origination_count** | **Integer** |  | [optional] 
**delegation_count** | **Integer** |  | [optional] 
**delegated_count** | **Integer** |  | [optional] 
**endorsement_count** | **Integer** |  | [optional] 
**first_seen** | **DateTime** |  | [optional] 
**last_seen** | **DateTime** |  | [optional] 
**name** | **String** |  | [optional] 
**balance** | **String** |  | [optional] 
**total_sent** | **String** |  | [optional] 
**total_received** | **String** |  | [optional] 
**baked_blocks** | **Integer** |  | [optional] 
**image_url** | **String** |  | [optional] 


