# TezexClient::ChainStatus

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**hash** | **String** |  | [optional] 
**chain_status** | **String** |  | [optional] 
**level** | **Integer** |  | [optional] 


