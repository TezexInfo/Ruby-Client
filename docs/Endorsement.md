# TezexClient::Endorsement

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**hash** | **String** |  | [optional] 
**source** | **String** |  | [optional] 
**level** | **Integer** |  | [optional] 
**block** | **String** |  | [optional] 
**endorsed_block** | **String** |  | [optional] 
**slot** | **Integer** |  | [optional] 
**time** | **DateTime** |  | [optional] 


