# TezexClient::BlockOperationsSorted

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**transactions** | [**Array&lt;Transaction&gt;**](Transaction.md) |  | [optional] 
**originations** | [**Array&lt;Origination&gt;**](Origination.md) |  | [optional] 
**delegations** | [**Array&lt;Delegation&gt;**](Delegation.md) |  | [optional] 
**endorsements** | [**Array&lt;Endorsement&gt;**](Endorsement.md) |  | [optional] 


