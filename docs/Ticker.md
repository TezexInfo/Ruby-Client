# TezexClient::Ticker

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**btc** | **String** |  | [optional] 
**cny** | **String** |  | [optional] 
**usd** | **String** |  | [optional] 
**eur** | **String** |  | [optional] 


