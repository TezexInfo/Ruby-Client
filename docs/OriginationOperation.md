# TezexClient::OriginationOperation

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**kind** | **String** |  | [optional] 
**manager_pubkey** | **String** |  | [optional] 
**balance** | **Integer** |  | [optional] 
**spendable** | **BOOLEAN** |  | [optional] 
**delegateable** | **BOOLEAN** |  | [optional] 
**delegate** | **String** |  | [optional] 
**script** | [**TezosScript**](TezosScript.md) |  | [optional] 
**storage** | [**TezosScript**](TezosScript.md) |  | [optional] 


