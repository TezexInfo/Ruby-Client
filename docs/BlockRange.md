# TezexClient::BlockRange

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**max_block_level** | **Integer** |  | [optional] 
**blocks** | [**Array&lt;Block&gt;**](Block.md) |  | [optional] 


