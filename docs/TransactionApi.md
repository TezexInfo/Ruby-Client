# TezexClient::TransactionApi

All URIs are relative to *http://betaapi.tezex.info/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_transaction**](TransactionApi.md#get_transaction) | **GET** /transaction/{transaction_hash} | Get Transaction
[**get_transactions_recent**](TransactionApi.md#get_transactions_recent) | **GET** /transactions/recent | Returns the last 50 Transactions
[**transactions_all**](TransactionApi.md#transactions_all) | **GET** /transactions/all | Get All Transactions
[**transactions_by_level_range**](TransactionApi.md#transactions_by_level_range) | **GET** /transactions/{startlevel}/{stoplevel} | Get All Transactions for a specific Level-Range


# **get_transaction**
> Transaction get_transaction(transaction_hash)

Get Transaction

Get a specific Transaction

### Example
```ruby
# load the gem
require 'tezex_client'

api_instance = TezexClient::TransactionApi.new

transaction_hash = "transaction_hash_example" # String | The hash of the Transaction to retrieve


begin
  #Get Transaction
  result = api_instance.get_transaction(transaction_hash)
  p result
rescue TezexClient::ApiError => e
  puts "Exception when calling TransactionApi->get_transaction: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **transaction_hash** | **String**| The hash of the Transaction to retrieve | 

### Return type

[**Transaction**](Transaction.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json



# **get_transactions_recent**
> Array&lt;Transaction&gt; get_transactions_recent

Returns the last 50 Transactions

### Example
```ruby
# load the gem
require 'tezex_client'

api_instance = TezexClient::TransactionApi.new

begin
  #Returns the last 50 Transactions
  result = api_instance.get_transactions_recent
  p result
rescue TezexClient::ApiError => e
  puts "Exception when calling TransactionApi->get_transactions_recent: #{e}"
end
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**Array&lt;Transaction&gt;**](Transaction.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json



# **transactions_all**
> TransactionRange transactions_all(opts)

Get All Transactions

Get all Transactions

### Example
```ruby
# load the gem
require 'tezex_client'

api_instance = TezexClient::TransactionApi.new

opts = { 
  page: 3.4, # Float | Pagination, 200 tx per page max
  order: "order_example", # String | ASC or DESC
  limit: 56 # Integer | Results per Page
}

begin
  #Get All Transactions
  result = api_instance.transactions_all(opts)
  p result
rescue TezexClient::ApiError => e
  puts "Exception when calling TransactionApi->transactions_all: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **Float**| Pagination, 200 tx per page max | [optional] 
 **order** | **String**| ASC or DESC | [optional] 
 **limit** | **Integer**| Results per Page | [optional] 

### Return type

[**TransactionRange**](TransactionRange.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json



# **transactions_by_level_range**
> TransactionRange transactions_by_level_range(startlevel, stoplevel, opts)

Get All Transactions for a specific Level-Range

Get all Transactions for a specific Level-Range

### Example
```ruby
# load the gem
require 'tezex_client'

api_instance = TezexClient::TransactionApi.new

startlevel = 3.4 # Float | lowest blocklevel to return

stoplevel = 3.4 # Float | highest blocklevel to return

opts = { 
  page: 3.4, # Float | Pagination, 200 tx per page max
  order: "order_example", # String | ASC or DESC
  limit: 56 # Integer | Results per Page
}

begin
  #Get All Transactions for a specific Level-Range
  result = api_instance.transactions_by_level_range(startlevel, stoplevel, opts)
  p result
rescue TezexClient::ApiError => e
  puts "Exception when calling TransactionApi->transactions_by_level_range: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **startlevel** | **Float**| lowest blocklevel to return | 
 **stoplevel** | **Float**| highest blocklevel to return | 
 **page** | **Float**| Pagination, 200 tx per page max | [optional] 
 **order** | **String**| ASC or DESC | [optional] 
 **limit** | **Integer**| Results per Page | [optional] 

### Return type

[**TransactionRange**](TransactionRange.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json



