# TezexClient::NetworkApi

All URIs are relative to *http://betaapi.tezex.info/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**network**](NetworkApi.md#network) | **GET** /network | Get Network Information


# **network**
> NetworkInfo network

Get Network Information

Get Network Information

### Example
```ruby
# load the gem
require 'tezex_client'

api_instance = TezexClient::NetworkApi.new

begin
  #Get Network Information
  result = api_instance.network
  p result
rescue TezexClient::ApiError => e
  puts "Exception when calling NetworkApi->network: #{e}"
end
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**NetworkInfo**](NetworkInfo.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json



