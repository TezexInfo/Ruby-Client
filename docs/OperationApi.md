# TezexClient::OperationApi

All URIs are relative to *http://betaapi.tezex.info/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_operation**](OperationApi.md#get_operation) | **GET** /operation/{operation_hash} | Get Operation


# **get_operation**
> Operation get_operation(operation_hash)

Get Operation

Get a specific Operation

### Example
```ruby
# load the gem
require 'tezex_client'

api_instance = TezexClient::OperationApi.new

operation_hash = "operation_hash_example" # String | The hash of the Operation to retrieve


begin
  #Get Operation
  result = api_instance.get_operation(operation_hash)
  p result
rescue TezexClient::ApiError => e
  puts "Exception when calling OperationApi->get_operation: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **operation_hash** | **String**| The hash of the Operation to retrieve | 

### Return type

[**Operation**](Operation.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json



