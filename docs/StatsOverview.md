# TezexClient::StatsOverview

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**price_usd** | **String** |  | [optional] 
**price_btc** | **String** |  | [optional] 
**block_time** | **Integer** | Blocktime in seconds | [optional] 
**priority** | **Float** |  | [optional] 


