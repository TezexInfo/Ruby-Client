# TezexClient::StatsApi

All URIs are relative to *http://betaapi.tezex.info/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_statistics**](StatsApi.md#get_statistics) | **GET** /stats/{group}/{stat}/{period} | Get Statistics
[**get_stats_overview**](StatsApi.md#get_stats_overview) | **GET** /stats/overview | Returns some basic Info


# **get_statistics**
> Array&lt;Stats&gt; get_statistics(group, stat, period, opts)

Get Statistics

Get Statistics

### Example
```ruby
# load the gem
require 'tezex_client'

api_instance = TezexClient::StatsApi.new

group = "group_example" # String | Block, Transaction, etc

stat = "stat_example" # String | 

period = "period_example" # String | 

opts = { 
  start_time: DateTime.parse("2013-10-20T19:20:30+01:00"), # DateTime | 
  end_time: DateTime.parse("2013-10-20T19:20:30+01:00") # DateTime | 
}

begin
  #Get Statistics
  result = api_instance.get_statistics(group, stat, period, opts)
  p result
rescue TezexClient::ApiError => e
  puts "Exception when calling StatsApi->get_statistics: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **group** | **String**| Block, Transaction, etc | 
 **stat** | **String**|  | 
 **period** | **String**|  | 
 **start_time** | **DateTime**|  | [optional] 
 **end_time** | **DateTime**|  | [optional] 

### Return type

[**Array&lt;Stats&gt;**](Stats.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json



# **get_stats_overview**
> StatsOverview get_stats_overview

Returns some basic Info

### Example
```ruby
# load the gem
require 'tezex_client'

api_instance = TezexClient::StatsApi.new

begin
  #Returns some basic Info
  result = api_instance.get_stats_overview
  p result
rescue TezexClient::ApiError => e
  puts "Exception when calling StatsApi->get_stats_overview: #{e}"
end
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**StatsOverview**](StatsOverview.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json



