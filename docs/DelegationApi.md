# TezexClient::DelegationApi

All URIs are relative to *http://betaapi.tezex.info/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_delegation**](DelegationApi.md#get_delegation) | **GET** /delegation/{delegation_hash} | Get Delegation


# **get_delegation**
> Delegation get_delegation(delegation_hash)

Get Delegation

Get a specific Delegation

### Example
```ruby
# load the gem
require 'tezex_client'

api_instance = TezexClient::DelegationApi.new

delegation_hash = "delegation_hash_example" # String | The hash of the Origination to retrieve


begin
  #Get Delegation
  result = api_instance.get_delegation(delegation_hash)
  p result
rescue TezexClient::ApiError => e
  puts "Exception when calling DelegationApi->get_delegation: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **delegation_hash** | **String**| The hash of the Origination to retrieve | 

### Return type

[**Delegation**](Delegation.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json



