# TezexClient::Transaction

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**hash** | **String** |  | [optional] 
**branch** | **String** |  | [optional] 
**source** | **String** |  | [optional] 
**public_key** | **String** |  | [optional] 
**level** | **Integer** |  | [optional] 
**block_hash** | **String** |  | [optional] 
**counter** | **Integer** |  | [optional] 
**time** | **DateTime** |  | [optional] 
**operations** | [**Array&lt;TransactionOperation&gt;**](TransactionOperation.md) |  | [optional] 


