# TezexClient::BlockApi

All URIs are relative to *http://betaapi.tezex.info/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**blocks_all**](BlockApi.md#blocks_all) | **GET** /blocks/all | Get All Blocks 
[**blocks_by_level**](BlockApi.md#blocks_by_level) | **GET** /blocks/{level} | Get All Blocks for a specific Level
[**blocks_by_level_range**](BlockApi.md#blocks_by_level_range) | **GET** /blocks/{startlevel}/{stoplevel} | Get All Blocks for a specific Level-Range
[**get_block**](BlockApi.md#get_block) | **GET** /block/{blockhash} | Get Block By Blockhash
[**get_block_delegations**](BlockApi.md#get_block_delegations) | **GET** /block/{blockhash}/operations/delegations | Get Delegations of a Block
[**get_block_endorsements**](BlockApi.md#get_block_endorsements) | **GET** /block/{blockhash}/operations/endorsements | Get Endorsements of a Block
[**get_block_operations_sorted**](BlockApi.md#get_block_operations_sorted) | **GET** /block/{blockhash}/operations | Get operations of a block, sorted
[**get_block_originations**](BlockApi.md#get_block_originations) | **GET** /block/{blockhash}/operations/originations | Get Originations of a Block
[**get_block_transaction**](BlockApi.md#get_block_transaction) | **GET** /block/{blockhash}/operations/transactions | Get Transactions of Block
[**recent_blocks**](BlockApi.md#recent_blocks) | **GET** /blocks/recent | returns the last 25 blocks


# **blocks_all**
> BlocksAll blocks_all(opts)

Get All Blocks 

Get all Blocks

### Example
```ruby
# load the gem
require 'tezex_client'

api_instance = TezexClient::BlockApi.new

opts = { 
  page: 3.4, # Float | Pagination, 200 tx per page max
  order: "order_example", # String | ASC or DESC
  limit: 56 # Integer | Results per Page
}

begin
  #Get All Blocks 
  result = api_instance.blocks_all(opts)
  p result
rescue TezexClient::ApiError => e
  puts "Exception when calling BlockApi->blocks_all: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **Float**| Pagination, 200 tx per page max | [optional] 
 **order** | **String**| ASC or DESC | [optional] 
 **limit** | **Integer**| Results per Page | [optional] 

### Return type

[**BlocksAll**](BlocksAll.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json



# **blocks_by_level**
> Array&lt;Block&gt; blocks_by_level(level)

Get All Blocks for a specific Level

Get all Blocks for a specific Level

### Example
```ruby
# load the gem
require 'tezex_client'

api_instance = TezexClient::BlockApi.new

level = 3.4 # Float | The level of the Blocks to retrieve, includes abandoned


begin
  #Get All Blocks for a specific Level
  result = api_instance.blocks_by_level(level)
  p result
rescue TezexClient::ApiError => e
  puts "Exception when calling BlockApi->blocks_by_level: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **level** | **Float**| The level of the Blocks to retrieve, includes abandoned | 

### Return type

[**Array&lt;Block&gt;**](Block.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json



# **blocks_by_level_range**
> BlockRange blocks_by_level_range(startlevel, stoplevel)

Get All Blocks for a specific Level-Range

Get all Blocks for a specific Level-Range

### Example
```ruby
# load the gem
require 'tezex_client'

api_instance = TezexClient::BlockApi.new

startlevel = 3.4 # Float | lowest blocklevel to return

stoplevel = 3.4 # Float | highest blocklevel to return


begin
  #Get All Blocks for a specific Level-Range
  result = api_instance.blocks_by_level_range(startlevel, stoplevel)
  p result
rescue TezexClient::ApiError => e
  puts "Exception when calling BlockApi->blocks_by_level_range: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **startlevel** | **Float**| lowest blocklevel to return | 
 **stoplevel** | **Float**| highest blocklevel to return | 

### Return type

[**BlockRange**](BlockRange.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json



# **get_block**
> Block get_block(blockhash)

Get Block By Blockhash

Get a block by its hash

### Example
```ruby
# load the gem
require 'tezex_client'

api_instance = TezexClient::BlockApi.new

blockhash = "blockhash_example" # String | The hash of the Block to retrieve


begin
  #Get Block By Blockhash
  result = api_instance.get_block(blockhash)
  p result
rescue TezexClient::ApiError => e
  puts "Exception when calling BlockApi->get_block: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **blockhash** | **String**| The hash of the Block to retrieve | 

### Return type

[**Block**](Block.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json



# **get_block_delegations**
> Array&lt;Delegation&gt; get_block_delegations(blockhash)

Get Delegations of a Block

Get all Delegations of a specific Block

### Example
```ruby
# load the gem
require 'tezex_client'

api_instance = TezexClient::BlockApi.new

blockhash = "blockhash_example" # String | Blockhash


begin
  #Get Delegations of a Block
  result = api_instance.get_block_delegations(blockhash)
  p result
rescue TezexClient::ApiError => e
  puts "Exception when calling BlockApi->get_block_delegations: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **blockhash** | **String**| Blockhash | 

### Return type

[**Array&lt;Delegation&gt;**](Delegation.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json



# **get_block_endorsements**
> Array&lt;Endorsement&gt; get_block_endorsements(blockhash)

Get Endorsements of a Block

Get all Endorsements of a specific Block

### Example
```ruby
# load the gem
require 'tezex_client'

api_instance = TezexClient::BlockApi.new

blockhash = "blockhash_example" # String | Blockhash


begin
  #Get Endorsements of a Block
  result = api_instance.get_block_endorsements(blockhash)
  p result
rescue TezexClient::ApiError => e
  puts "Exception when calling BlockApi->get_block_endorsements: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **blockhash** | **String**| Blockhash | 

### Return type

[**Array&lt;Endorsement&gt;**](Endorsement.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json



# **get_block_operations_sorted**
> BlockOperationsSorted get_block_operations_sorted(blockhash)

Get operations of a block, sorted

Get the maximum Level we have seen, Blocks at this level may become abandoned Blocks later on

### Example
```ruby
# load the gem
require 'tezex_client'

api_instance = TezexClient::BlockApi.new

blockhash = "blockhash_example" # String | The hash of the Block to retrieve


begin
  #Get operations of a block, sorted
  result = api_instance.get_block_operations_sorted(blockhash)
  p result
rescue TezexClient::ApiError => e
  puts "Exception when calling BlockApi->get_block_operations_sorted: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **blockhash** | **String**| The hash of the Block to retrieve | 

### Return type

[**BlockOperationsSorted**](BlockOperationsSorted.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json



# **get_block_originations**
> Array&lt;Origination&gt; get_block_originations(blockhash)

Get Originations of a Block

Get all Originations of a spcific Block

### Example
```ruby
# load the gem
require 'tezex_client'

api_instance = TezexClient::BlockApi.new

blockhash = "blockhash_example" # String | Blockhash


begin
  #Get Originations of a Block
  result = api_instance.get_block_originations(blockhash)
  p result
rescue TezexClient::ApiError => e
  puts "Exception when calling BlockApi->get_block_originations: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **blockhash** | **String**| Blockhash | 

### Return type

[**Array&lt;Origination&gt;**](Origination.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json



# **get_block_transaction**
> Array&lt;Transaction&gt; get_block_transaction(blockhash)

Get Transactions of Block

Get all Transactions of a spcific Block

### Example
```ruby
# load the gem
require 'tezex_client'

api_instance = TezexClient::BlockApi.new

blockhash = "blockhash_example" # String | Blockhash


begin
  #Get Transactions of Block
  result = api_instance.get_block_transaction(blockhash)
  p result
rescue TezexClient::ApiError => e
  puts "Exception when calling BlockApi->get_block_transaction: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **blockhash** | **String**| Blockhash | 

### Return type

[**Array&lt;Transaction&gt;**](Transaction.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json



# **recent_blocks**
> Array&lt;Block&gt; recent_blocks

returns the last 25 blocks

Get all Blocks for a specific Level

### Example
```ruby
# load the gem
require 'tezex_client'

api_instance = TezexClient::BlockApi.new

begin
  #returns the last 25 blocks
  result = api_instance.recent_blocks
  p result
rescue TezexClient::ApiError => e
  puts "Exception when calling BlockApi->recent_blocks: #{e}"
end
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**Array&lt;Block&gt;**](Block.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json



